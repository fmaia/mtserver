package br.edu.ifce.ps2.mtserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MTServer implements Runnable {
	public Socket cliente;
	@Override
	public void run() {
		try {
			ObjectInputStream entrada = 
				new ObjectInputStream(
					cliente.getInputStream());
			ObjectOutputStream saida = 
				new ObjectOutputStream(
					cliente.getOutputStream());
			long n = entrada.readLong();
			
			saida.writeLong(2*n);
			saida.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			ServerSocket server = 
					new ServerSocket(12347);
			while(true) {
				Socket so = server.accept();
				MTServer serverOp = new MTServer();
				serverOp.cliente = so;
				Thread t = new Thread(serverOp);
				t.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}










