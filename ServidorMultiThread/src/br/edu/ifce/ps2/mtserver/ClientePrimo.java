package br.edu.ifce.ps2.mtserver;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

public class ClientePrimo {

	public static void main(String[] args) {
		try {
			Socket cliente;
			InetAddress serverIP = 
				InetAddress.getByAddress(
						new byte[] {127,0,0,1});
			cliente = new Socket(serverIP, 12347);
			System.out.println(
					"Conexão estabelecida");
			ObjectOutputStream saida = 
				new ObjectOutputStream(
					cliente.getOutputStream());
			ObjectInputStream entrada =
				new ObjectInputStream(
						cliente.getInputStream());
			String valor = 
				JOptionPane.
				showInputDialog("Valor:");
			saida.writeLong(
					Long.parseLong(valor));
			saida.flush();
			long resposta = entrada.readLong();
			JOptionPane.showMessageDialog(
					null, resposta);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
